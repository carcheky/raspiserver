#!/usr/bin/with-contenv bash
curl https://raw.githubusercontent.com/carcheky/arr-scripts/main/radarr/setup.bash | bash
cp -f /custom-cont-init.d/conf/extended.conf /config/extended.conf
cp -f /custom-cont-init.d/conf/recyclarr.yaml /config/extended/recyclarr.yaml
